const postEstudent = async (res) => {
  let cedula = document.getElementById("cedula").value;
  let apellido = document.getElementById("apellido").value;
  let nombre = document.getElementById("nombre").value;
  let direccion = document.getElementById("direccion").value;
  let semestre = document.getElementById("semestre").value;
  let paralelo = document.getElementById("paralelo").value;
  let email = document.getElementById("email").value;
  if (validar(cedula, apellido, nombre, direccion, semestre, paralelo, email)) {
    try {
      await pool.query(
        "INSERT INTO  estudiante (cedula,apellido_es,nombre_es,direccion,semestre,paralelo,email)VALUES ($1,$2,$3,$4,$5,$6,$7)",
        [cedula, apellido, nombre, direccion, semestre, paralelo, email]
      );
    } catch (error) {
      console.log(error.message);
      res.json({ error: error.message });
    }
  }
};

function validar(cedula, apellido, nombre, direccion, semestre, paralelo, email) {
    if (cedula!="" && apellido !="" && nombre!="" && direccion!="" && semestre!="" && paralelo!="" && email){
        return true;
    }
    return false;
}
